package dev.home.advpush;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

import dev.home.advpush.tcpconnection.TCPConnection;
import dev.home.advpush.tcpconnection.TCPConnectionListener;

public class MainActivity extends AppCompatActivity {

    private TCPConnection connection;
    private Button btn_send, btn_get_count, btn_con_disk;
    private CheckBox chb_admob, chb_startapp, chb_show_adv, chb_show_notif;
    private EditText edt_link, edt_notif_title, edt_notif_content;
    private TextView tvUsersOnline;
    private boolean bln_is_connected = false;
    private String adv_choose = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new MyAsyncThread().execute();

        btn_send = (Button)findViewById(R.id.btn_send_obj);
        btn_get_count = (Button)findViewById(R.id.btn_users_online);
        btn_con_disk = (Button)findViewById(R.id.btn_con_disk);

        chb_admob = (CheckBox)findViewById(R.id.chb_admob);
        chb_startapp = (CheckBox)findViewById(R.id.chb_startapp);
        chb_show_adv = (CheckBox)findViewById(R.id.chb_show_adv);
        chb_show_notif = (CheckBox)findViewById(R.id.chb_show_notif);

        tvUsersOnline = (TextView) findViewById(R.id.tv_users_online);

        edt_link = (EditText)findViewById(R.id.edt_link);
        edt_notif_title = (EditText)findViewById(R.id.edt_title);
        edt_notif_content = (EditText)findViewById(R.id.edt_content);

        btn_con_disk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!bln_is_connected){
                    new MyAsyncThread().execute();
                } else {
                    if(connection != null) connection.disconnect();
                }
            }
        });

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adv_choose = "";
                if(chb_admob.isChecked()){
                    adv_choose = "admob";
                }else if (chb_startapp.isChecked()){
                    adv_choose = "start_app";
                }
                printToLogs(adv_choose);

                connection.sendString(new AdvJsonObj(
                        chb_show_adv.isChecked(),
                        adv_choose,
                        chb_show_notif.isChecked(),
                        edt_link.getText().toString(),
                        edt_notif_title.getText().toString(),
                        edt_notif_content.getText().toString()).getStringFromJson());
            }
        });


        btn_get_count.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                connection.sendString("get_count");
            }
        });
    }



    class MyAsyncThread extends AsyncTask<Void, Void, String> implements TCPConnectionListener{

        @Override
        protected String doInBackground(Void... params) {
            try {
                connection = new TCPConnection(this, "freetv.com.ua", 8189);
            }catch (IOException e){
                printToLogs("error");
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }

        @Override
        public void onReceiveString(TCPConnection tcpConnection, final String value) {
            if(value != null && isInteger(value)){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tvUsersOnline.setText(value);
                    }
                });

            }
        }

        @Override
        public void onConnectionReady(TCPConnection tcpConnection) {
            printToLogs("onConnectionReady");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    bln_is_connected = true;
                    btn_con_disk.setText("Connected");
                }
            });
        }

        @Override
        public void onException(TCPConnection tcpConnection, Exception e) {
            printToLogs("onException");
        }

        @Override
        public void onDisconnect(TCPConnection tcpConnection) {
            printToLogs("onDisconnect");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    btn_con_disk.setText("Disconnected");
                    bln_is_connected = false;
                }
            });

        }
    }

    public synchronized void printToLogs(String string) {
        Log.d("log", string);
    }

    public synchronized String getPrintStackTrace(Exception e){
        Writer writer = new StringWriter();
        PrintWriter printWriter = new PrintWriter(writer);
        e.printStackTrace(printWriter);
        return writer.toString();
    }

    public boolean isInteger( String input ) {
        try {
            Integer.parseInt( input );
            return true;
        }
        catch( Exception e ) {
            return false;
        }
    }
}
