package dev.home.advpush;


import android.util.Log;

import org.json.JSONObject;

public class AdvJsonObj {

    private JSONObject jsonObject = new JSONObject();
    private String show_adv = "show_adv";
    private String adv_type = "adv_type";
    private String link = "link";
    private String show_notif = "show_notif";
    private String void_string = "void";
    private String notifTitle = "notif_title";
    private String notifContent = "notif_content";
    private String appStatus = "app_status";
    private String packageName = "package_name";

    public AdvJsonObj(String getStJsonObj){
        try {
            jsonObject = new JSONObject(getStJsonObj);
        }catch (Exception e){
            Log.d("log", "Error");
        }
    }

    public AdvJsonObj(boolean _app_status, String _package_name){
        try {
            jsonObject.put(show_adv,void_string);
            jsonObject.put(adv_type, void_string);
            jsonObject.put(show_notif, void_string);
            jsonObject.put(link, void_string);
            jsonObject.put(notifTitle, void_string);
            jsonObject.put(notifContent, void_string);
            jsonObject.put(appStatus, _app_status);
            jsonObject.put(packageName, _package_name);
        }catch (Exception e){
            Log.d("log", "Error");
        }
    }

    public AdvJsonObj(Boolean _show_adv, String _adv_type, Boolean _show_notif, String _link, String _notif_title, String _notif_content){
        try {
            jsonObject.put(show_adv, _show_adv);
            jsonObject.put(adv_type, _adv_type);
            jsonObject.put(show_notif, _show_notif);
            jsonObject.put(link, _link);
            jsonObject.put(notifTitle, _notif_title);
            jsonObject.put(notifContent, _notif_content);
        }catch (Exception e){
            Log.d("log", "Error");
        }
    }

    public String getStringFromJson(){
        return jsonObject.toString();
    }

    public boolean getBlnShowAdv(){
        try {
            return jsonObject.getBoolean(show_adv);
        }catch (Exception e){
            Log.d("log", "Error");
            return false;
        }
    }

    public boolean getBlnShowNotif(){
        try {
            return jsonObject.getBoolean(show_notif);
        }catch (Exception e){
            Log.d("log", "Error");
            return false;
        }
    }

    public String getAdvType(){
        try {
            return jsonObject.getString(adv_type);
        }catch (Exception e){
            Log.d("log", "Error");
            return void_string;
        }
    }

    public String getLink(){
        try {
            return jsonObject.getString(link);
        }catch (Exception e){
            Log.d("log", "Error");
            return void_string;
        }
    }

    public String getNotifText(){
        try {
            return jsonObject.getString(notifTitle);
        }catch (Exception e){
            Log.d("log", "Error");
            return void_string;
        }
    }

    public String getNotifContent(){
        try {
            return jsonObject.getString(notifContent);
        }catch (Exception e){
            Log.d("log", "Error");
            return void_string;
        }
    }

    public boolean getAppStatus(){
        try {
            return jsonObject.getBoolean(appStatus);
        }catch (Exception e){
            Log.d("log", "Error");
            return false;
        }
    }

    public String getPackageName(){
        try {
            return jsonObject.getString(packageName);
        }catch (Exception e){
            Log.d("log", "Error");
            return void_string;
        }
    }
}
